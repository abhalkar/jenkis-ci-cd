# Jenkis

## What is jenkins and contineos devoplement/divivery/intergation  ?

- In software engineering, CI/CD or CICD is the combined practices of continuous integration and continuous delivery or continuous deployment. CI/CD bridges the gaps between development and operation activities and teams by enforcing automation in building, testing and deployment of applications

- Contineous intigration 
    - all the phase are automated plan code 
    - (Developer) devop code --> then we devloy on cloud (Deploy) 
    - Git (code face) --> Integration(Git/gitlab---> testing -->deploy)
-stages
    - Build     #we are autmoating all build test deploy features
    - test
    - deploy

## what is Jenkins (Java based application)
- Jenkins is an open source automation server. It helps automate the parts of software development related to building, testing, and deploying, facilitating continuous integration and continuous delivery. It is a server-based system that runs in servlet containers such as Apache Tomcat.

- Benifits
    - plugins #addon feature
        - Plugins are the primary means of enhancing the functionality of a Jenkins environment to suit organization- or user-specific needs. There are over a thousand different plugins which can be installed on a Jenkins controller and to integrate various build tools, cloud providers, analysis tools, and much more.
        - extension 
- Why to use jenkins 
    - Provides lot of feature like plugins also CI/CD 
## lets see how we use jenkins now 
### Creating a Jobs
- navigate -> New items -> 

- people 
    - all people
- build histroy
    - execcutions is noting but build 
    - we can build history
- Manage Jenkins 
    - We can see the path 
    - no of exicutors
        - how many jobs we can exicute paralley or in cup how many job we can perfom parallely
        - no core vs no of exicutors
    - labels 
        - labels are nothing but tags 
        - labels are used when we want to exicute it on perticular node or master
    - usage
        - use master as much you can 
        - need lable to exicute job on master
    - global tool ocnfig
        - we will use application here 
    - manage plugis 
    - manage node 
        - manage the slaves/node/agents
    -secuity
## In Jenkins we follow Master-Slave concept
- wher we have one master and multiple slaves
- we can also make multiple master as well for backup puorpose
- Jenkins is a single server (now we need to exicute our job)
- so we will exicute all jobs in different pc so that we get max spped for master

### Now lets create a job printing hello world
- now we are writing free style project
- accoring to project we will need plugin 
    - creating job 
        - Nevigate -> newiteam-> enter project name -> frestyle -> ok -> build(Exicute shell) -> echo "hello world" -> save
    - Now build job 
        - nevigate -> Build now -> select build histroy -> console o/p

### Why we need to create a job
    - when devopler push their code to repo we need to test and delpoy it using the job

### create a new job for pull the code form repo 
    - we will need git to be install on master server hence we are running all cmd on master```yum install git```
    - we will also need plugin 
        - nevigate -> manage jenkins -> manage plugins ->avilable -> git -> install without restart
        - we have many plugins 100 
    - we will need authenticate for cloning repo
    - so we need to keep key in jenkins 
    - workspace where we work we can see our repository is cloned
    - we can check the pulled repo on master server check```var/lib/jenkins/workspace/codepull```

### lets create the Master-Slave concept
    - so we can distriue the build to node1 
    - we can also use maven on different node2 
    - for sonarqube we will use node3
    - we can alos create the more than one master for backup perpose
    - launch the new instance on ec2 which will work as slave/node1
        - yum install git
        - yum install java-11-openjdk
    - here ssh will communicate between servre and node
    - nevigate to manage jneknis -> manage nodes and cloud 
    - install java-11-openjdk in node system for connection or keeping up the node
    - create a ssh-key pair on node side so we can connect through ssh 

### what is build trigger
    - we can set trigeer for below
        - trigger builds remotely (e.g., from scripts)
        - Build after other projects are built
        - Build periodically
        - Poll SCM

# Now we will create a pipeline in jenkins 
    - what is pipeline is nothing but no of task exicuted in sequence/parellel
    - Cloth washing example.
    - we will create diff stage 
        - PULL -> Build -> TEST -> DEPLOY 
        - postbuild action after build stage we will sent o/p to test.
## Now we will try to run our pipeline through gitlab repo
    - select the pipeline script form scm 
    - script path (pieplines/piepline.jdp)
    - jpd is only for declerative pipeline
- pipeline 
    - we perform ci/cd using th pipeline
    - written in GRROVY language also called as dsl(domain specific pipeline) langauge 
    - we use two types of pipeline
        - Declarative pipeline(new)
        ---

            pipeline {
            agent { docker { image 'maven:3.8.4-openjdk-11-slim' } }
            stages {
                stage('build') {
                    steps {
                        sh 'mvn --version'
                    }
                }
            }
        ...
        - scripted pipeline (old) 
            - here we will pull repo from our gitlab
        ---
            pipeline {
            agent any 
            stages {
                stage('pull') { 
                    steps {
                        git branch: 'main', credentialsId: 'gitlab_cread', url: 'https://gitlab.com/abhalkar/jenkis-ci-cd.git' 
                    }
                }
            stage('build') { 
                steps {
                    echo "Build Successul"
                    }
                }
            }
        ...


 

    





    


